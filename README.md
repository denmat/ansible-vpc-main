Using Ansible to create the main VPC.
---

What this creates
---

Main VPC with a single bastion node.

Use this main VPC as your bastion VPC to connect through to other VPCs.

Setup
---
We have some setup scripts in _./bin_
  * First install ansible (latest as possible).
```
$ ./bin/setup_ansible.sh
```
This will try its best to install ansible and also checkout the required upstream branch that I have for ansible that has the required
extra modules. (see [DenMat](https://github.com/denmat/ansible-modules-core "DenMat"))


To build packer images you will need to set the following:
  * export your aws credentials like so:

```
export AWS_ACCESS_KEY=<youraccessid> ; export AWS_SECRET_KEY=<yoursecret>
```

Production
---

```
ansible-playbook -i hosts/host.ini plays/build_vpc.yml -e @group_vars/production.yml --ask-vault-pass -e @secrets/production.yml
```

  * You will be asked for a password, use the one you have just decrypted from the toolbox.
  * This should build a vpc or confirm the VPC is already built and running.

Creating this VPC sets up a peering connection to ```main``` VPC. 


Testing
---
You can run _ansible_ against the test vpc and it should be an idempotent operation. Try it out.

If you wanted to test building your own vpc and didn't want to touch production then you would create a copy of the following files:

```
$ cp group_vars/production.yml group_vars/<your_env_name>.yml
```

Edit the file like the following:
```
$ git diff group_vars/test.yml group_vars/my_env.yml
diff --git a/group_vars/test.yml b/group_vars/my_env.yml
index 643decf..8bde78a 100644
--- a/group_vars/test.yml
+++ b/group_vars/my_env.yml
@@ -1,4 +1,4 @@
-this_environment: test
+this_environment: myenv
```

You my want to change any credentials to access particular things, like dbs. If that is the case you can use ```ansible-vault```
to make any suitable changes - or in the example below you could use the ```@secrets/test.yml``` file.


Change any particular environment variables you think are approriate and then run something like this:
```
ansible-playbook -i hosts/host.ini plays/build_vpc.yml -e @group_vars/my_env.yml --ask-vault-pass -e @secrets/my_env.yml

```

Running adhoc commands
---

```ansible.cfg``` expects a local ```.ssh/config``` file. Change this to whatever you wish and then you can issue commands like below:

```
$ ansible -m service -a "name=ssh state=running" tag_Role_bastion
52.64.116.248 | success >> {
    "changed": false,
    "name": "ssh",
    "state": "started"
}
```

KNOWN ISSUES:
---
```
ERROR: ec2_ami_find is not a legal parameter in an Ansible task or handler
```
  * you will need to run the ```./bin/upstream_ansible.sh``` to include the upstream ansible-modules-core I have merged in.
