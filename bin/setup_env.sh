#!/bin/bash

PATH=/usr/local/rbenv/bin:/usr/local/rbenv/shims:$PATH
source /etc/profile.d/rbenv.sh

rbenv shell 2.1.5
