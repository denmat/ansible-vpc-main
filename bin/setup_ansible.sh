#!/bin/bash

if [[ `uname` == 'Darwin' ]] ; then
  brew list python &> /dev/null || brew install python
  pip list -q ansible || pip install ansible
elif [[ `lsb_release -si` == 'Ubuntu' ]] ; then 
  echo "Ubuntu"
  dpkg -l ansible || bash -c 'sudo apt-get install -y software-properties-common \
      && sudo apt-add-repository -y ppa:ansible/ansible \
      && sudo aptitude update \
      && sudo aptitude install ansible python-apt -y'
fi

[[ -d ./lib/ansible-modules-core ]] || ./bin/upstream_ansible.sh
