require 'spec_helper'

describe 'base_image' do
  include_examples 'base::init'
end

describe package('libpq-dev') do
  it { should be_installed }
end

describe package('ruby-dev') do
  it { should be_installed }
end

describe package('build-essential') do
  it { should be_installed }
end

describe package('node') do
  it { should be_installed }
end  
