shared_examples 'base::init' do

  describe package('curl') do
    it { should be_installed }
  end
  
  describe package('git') do
    it { should be_installed }
  end

end
