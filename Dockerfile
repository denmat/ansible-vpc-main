FROM ubuntu:14.04
MAINTAINER: tu2Bgone@gmail.com

ADD https://dl.bintray.com/mitchellh/consul/0.5.0_linux_amd64.zip /tmp/consul
RUN [ 'cd /tm/consul && aptitude install -y unzip  && unzip 0.5.0_linux_amd64.zip' ]
RUN [ 'install -m 0755 consul /usr/local/bin' ]
EXPOSE 8300
CMD [ '/bin/bash' ]
